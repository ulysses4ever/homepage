---
author: bgamari
title: "GHC 8.6.5-rc1 released"
date: 2019-04-08
tags: release
---

The GHC team is proud to announce the first release candidate of 8.6.5.
The source distribution, binary distributions, and documentation are
available on
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.6.5-rc1).

This release fixes a handful of issues with 8.6.4:

 - Binary distributions once again ship with Haddock documentation including
   syntax highlighted source of core libraries (#16445)

 - A build system issue where use of GCC with `-flto` broke `configure`
   was fixed (#16440)
 
 - An bug affecting Windows platforms wherein XMM register values could be
   mangled when entering STG has been fixed (#16514)
 
 - Several Windows packaging issues resulting from the recent CI rework.
   (#16408, #16398, #16516)

Since this is a relatively small bugfix release we are bypassing the
alpha releases and moving right to the release candidate stage. If all
goes well the final release will be cut next week.

As always, if anything looks amiss do let us know.

Happy testing!


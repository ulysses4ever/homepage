---
author: thoughtpolice
title: "GHC Weekly News 2014/11/18"
date: 2014-11-18
tags: ghc news
---

Hello \*,

Once more we have the GHC Weekly news! This one is a bit late due to Austin being in limbo unexpectedly for a few days last week. (The next one will of course come again on Friday to keep things straight.)

With that out of the way, let's see what exactly is going on:

 - The STABLE freeze is happening at the end of this week! That means if you have something you want to get in, try to get people aware of it! Austin (yours truly) has a backed up review queue it would seem, but hopes to clear a bunch of it out before then.

 - Simon and Gergo started a whole bunch of discussion about type signatures for pattern synonyms. There is a surprising amount of content to talk about here for something that might seem simple: https://www.haskell.org/pipermail/ghc-devs/2014-November/007066.html

 - Herbert Valerio Riedel has finally landed `integer-gmp2`, AKA Phab:D86, which implements a complete overhaul of the `integer-gmp` library. This library will be switched on by default in GHC 7.10.1, which means the `integer-gmp` library version will have a super-major bump (version `1.0.0.0`). This is the beginning of a longer-term vision for more flexible `Integer` support in GHC, as described by Herbert on the design page: https://ghc.haskell.org/trac/ghc/wiki/Design/IntegerGmp2
  This implementation also fixes a long standing pain point where GHC would hook GMP allocations to exist on the GHC heap. Now GMP is just called to like any FFI library.

  - Jan Stolarek made a heads up to help out GHC newcomers: if you see a ticket that should be easy, please tag it with the `newcomer` keyword! This will let us have a live search of bugs that new developers can take over. (Incidentally, Joachim mentions this is the same thing Debian is doing in their bug tracker): https://www.haskell.org/pipermail/ghc-devs/2014-November/007313.html

  - Merijn Verstraaten has put forward a proposal for more flexible literate style Haskell file extensions. There doesn't seem to be any major opposition, just some questions about the actual specification and some other ramifications: https://www.haskell.org/pipermail/ghc-devs/2014-November/007319.html

  - Facundo Domínguez posed a question about CAFs in the GC, which Jost Berthold was fairly quick to reply to: https://www.haskell.org/pipermail/ghc-devs/2014-November/007353.html

  - Adam Gundry, Eric Seidel, and Iavor Diatchki have grouped together to get a new, unexpected feature into 7.10: type checking plugins. Now, GHC will be able to load a regular Haskell package as a plugin during the compilation process. Iavor has a work-in-progress plugin that solves constraints for type-level natural numbers using a SMT solver. The code review from everyone was published in Phab:D489.

  - Austin opened up a discussion about the future of the Haskell98 and Haskell2010 packages, and the unfortunate conclusion is it looks like we're going to drop them for 7.10. Austin has some rationale, and there was some followup in the mailing list thread too: https://www.haskell.org/pipermail/ghc-devs/2014-November/007357.html

Closed tickets this week include: #9785, #9053, #9513, #9073, #9077, #9683, #9662, #9646, #9787, #8672, #9791, #9781, #9621, #9594, #9066, #9527, #8100, #9064, #9204, #9788, #9794, #9608, #9442, #9428, #9763, #9664, #8750, #9796, #9341, #9330, #9323, #9322, #9749, #7381, #8701, #9286, #9802, #9800, #9302, #9174, #9171, #9141, #9100, #9134, #8798, #8756, #8716, #8688, #8680, #8664, #8647, #9804, #8620, #9801, #8559, #8559, #8545, #8528, #8544, #8558

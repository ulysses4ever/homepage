---
author: thoughtpolice
title: "GHC Weekly News - 2015/09/17"
date: 2015-09-17
tags: ghc news
---

Hi \*,

Welcome for the latest entry in the GHC Weekly News. It's been a little while, but here we are!

And your author has finally returned from his 8 week sabbatical, too! So without any futher ado, lets get going...

## 8.0.1 release roadmap

So `HEAD` has been steaming along pretty smoothly for the past few months now. After talking with Simon last week, we decided that the best course of action would be to release 8.0.1 (a super-major release) sometime around late February, which were the plans for 7.10 (modulo a few weeks due to the FTP debates). The current schedule is roughly:

 - November: Fork the new `ghc-8.0` STABLE branch
  - At this point, `master` development will probably slow as we fix bugs.
  - This gives us 2 months or so until branch, from Today.
  - This is nice as the branch is close to the first RC.
 - December: First release candidate
 - Mid/late February: Final release.

"**Why call it 8.0.1?**", you ask? Because we have a lot of excellent features planned! I'm particularly partial to Richard's work for merging types and kinds (Phab:D808). But there's a lot of other stuff.

For all the nitty gritty details, be sure to check [8.0.1 status page](https://gitlab.haskell.org/ghc/ghc/wikis/wiki:Status/GHC-8.0.1) to keep track of everything - it will be our prime source of information and coordination. And be sure to [read my email to `ghc-devs`](https://mail.haskell.org/pipermail/ghc-devs/2015-September/009952.html) for more info.

### ... and a 7.10.3 release perhaps?

On top of this, we've been wondering if another release in the 7.10 branch should be done. Ben did the release shortly after I left, and for the most part looked pretty great. But there have been some snags, as usual.

So we're asking: [who needs GHC 7.10.3?](https://mail.haskell.org/pipermail/ghc-devs/2015-September/009953.html) We'd really like to know of any major showstoppers you've found with 7.10 that are preventing you from using it. Especially if you're stuck or there's no clear workaround.

Currently, we're still not 100% committed to this course of action (since the release will take time away from other things). However, we'll keep the polls open for a while - so *please* get in touch with us if you need it! (Be sure to read my email for specific information.)

## List chatter

(Over the past two weeks)

  - Bartosz Nitka writes to `ghc-devs` about the ongoing work to try and fix deterministic compilation in GHC (the dreaded ticket #4012). There's a very detailed breakdown of the current problems and issues in play, with responses from others - https://mail.haskell.org/pipermail/ghc-devs/2015-September/009964.html

  - Richard Eisenberg wants to know - how can I download all of `Hackage` to play with it? GHC developers are surely interested in this, so they can find regressions quickly - https://mail.haskell.org/pipermail/ghc-devs/2015-September/009956.html

  - I wrote to the list about the upcoming tentative 7.10.3 plans, as I mentioned above. https://mail.haskell.org/pipermail/ghc-devs/2015-September/009953.html

  - I *also* wrote to the list about the tentative 8.0.1 plans, too. https://mail.haskell.org/pipermail/ghc-devs/2015-September/009952.html

  - Johan Tibell asks about his ongoing work for implementing unboxed sum types - in particular, converting unboxed sum types in `StgCmm`. https://mail.haskell.org/pipermail/ghc-devs/2015-September/009926.html

  - Ryan Scott wrote a proposal for the automatic derivation of `Lift` through GHC's deriving mechanism, specifically for `template-hasekll` users. The response was positive and the code is going through review now (Phab:D1168). https://mail.haskell.org/pipermail/ghc-devs/2015-September/009838.html

  - Andrew Gibiansky writes in with his own proposal for a new "Argument Do" syntax - a change which would allow `do` to appear in positions without `($)` or parenthesis, essentially changing the parser to insert parens as needed. The code is up at Phabricator for brave souls (Phab:D1219). https://mail.haskell.org/pipermail/ghc-devs/2015-September/009821.html

  - Edward Yang started a monstrous thread after some discussions at ICFP about a future for *unlifted* data types in GHC. These currently exist as special magic, but the proposals included would allow users to declare their own types as unlifted, and make unlifted values more flexible (allowing `newtype` for example). See wiki:UnliftedDataTypes and Edward's thread for more. https://mail.haskell.org/pipermail/ghc-devs/2015-September/009799.html

## Noteworthy commits

(Over the past several weeks)

  - Commit 374457809de343f409fbeea0a885877947a133a2 - **Injective Type Families**

  - Commit 8ecf6d8f7dfee9e5b1844cd196f83f00f3b6b879 - **Applicative Do notation**

  - Commit 6740d70d95cb81cea3859ff847afc61ec439db4f - Use IP-based CallStack in `error` and `undefined`

  - Commit 43eb1dc52a4d3cbba9617f5a26177b8251d84b6a - Show `MINIMAL` complete definition in GHCi's `:info`

  - Commit 296bc70b5ff6c853f2782e9ec5aa47a52110345e - Use a response file for linker command line arguments

  - Commit 4356dacb4a2ae29dfbd7126b25b72d89bb9db1b0 - Forbid annotations when Safe Haskell is enabled

  - Commit 7b211b4e5a38efca437d76ea442495370da7cc9a - Upgrade GCC/binutils to 5.2.0 release for Windows (i386/amd64)

## Closed tickets

(Over the past two weeks)

#10834, #10830, #10047, #9943, #1851, #1477, #8229, #8926, #8614, #10777, #8596, #10788, #9500, #9087, #10157, #10866, #10806, #10836, #10849, #10869, #10682, #10863, #10880, #10883, #10787, #8552, #10884, #7305, #5757, #9389, #8689, #10105, #8168, #9925, #10305, #4438, #9710, #10889, #10885, #10825, #10821, #10790, #10781, #9855, #9912, #10033, #9782, #10035, #9976, #10847, and #10865.

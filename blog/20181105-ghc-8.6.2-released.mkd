---
author: Ben Gamari
title: "GHC 8.6.2 released"
date: 2018-11-05
tags: release
---

The GHC team is very happy to announce the availability of GHC 8.6.2, a
bugfix release to GHC 8.6.1. The source distribution, binary
distributions, and documentation for this release are available at [haskell.org](https://downloads.haskell.org/~ghc/8.6.2).

The 8.6 release fixes several regressions present in 8.6.1 including:

 * A long-standing (but previously hard to manifest) bug resulting in
   undefined behavior for some applications of dataToTag# has been fixed
   (#15696)

 * An incorrect linker path to libgmp in the Mac OS binary distributions
   (#15404)

 * A regression rendering Windows installations to read-only directories
   unusable (#15667)

 * A regression resulting in panics while compiling some record updates
   of GADTs constructors  (#15499)

 * A regression resulting in incorrect type errors when splicing types
   into constraint contexts has been fixed (#15815)

 * Around a dozen other issues.

See [[|Trac](https://ghc.haskell.org/trac/ghc/query?status=closed&milestone=8.6.2&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=component&order=priority)] for a full list of issues resolved in this release.

Note that this release ships with one significant but long-standing bug
(#14251): Calls to functions taking both Float# and Double# may result
in incorrect code generation when compiled using the LLVM code generator.
This is not a new issue, it has existed as long as the LLVM code
generator has existed; however, changes in code generation in 8.6 made
it more likely that user code using only lifted types will trigger it.

Happy compiling!

Cheers,

* Ben

